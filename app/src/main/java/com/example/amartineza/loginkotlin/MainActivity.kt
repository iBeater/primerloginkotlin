package com.example.amartineza.loginkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.btnLogin.setOnClickListener {
            val name: String = this.etUser?.text?.toString()?.trim() ?: ""
            val pass: String = this.etPass?.text?.toString()?.trim() ?: ""

            if(name.isNotBlank() && pass.isNotBlank()){
                this.createDialog("Login","User: $name Pass: $pass").show()
            }
        }
    }
}
