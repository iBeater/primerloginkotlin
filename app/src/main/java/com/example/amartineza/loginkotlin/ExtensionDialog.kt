package com.example.amartineza.loginkotlin

import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity

/**
 * Created by amartineza on 3/28/2018.
 */

fun AppCompatActivity.createDialog(title: String, message: String) : AlertDialog{
    val builder = AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, null)
    return builder.create()
}